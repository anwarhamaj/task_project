const { getUsers,getUsersWithLimit } = require("../controller/getUsers");
const formData = require("multer")().array();
const router = require("express").Router();

router.get("/:userId/karma-position", formData, getUsers);
router.get("/:userId/karma-position/limit/:limit", formData, getUsersWithLimit);

module.exports = router;
