const { DataTypes, Model } = require("sequelize");
const sequelize = require("../util/mysql_database.js");
const User = require("./user.js");

require("dotenv").config();

class Image extends Model {}
Image.init(
  {
    // Model attributes are defined here
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    url: { type: DataTypes.STRING, allowNull: true },
  },
  {
    // Other model options go here
    sequelize, // We need to pass the connection instance
    modelName: "images", // We need to choose the model name
    timestamps: true,
  }
);
Image.hasOne(User);
User.belongsTo(Image, {
  constraints: true,
  onDelete: "CASCADE",
});
module.exports = Image;
