const express = require("express");
const app = express();
const path = require("path");
app.use("/image", express.static("upload/images"));
app.use(require("cors")());
app.use(express.json());
//app.use(express.urlencoded())
//---------------------------------------------------------------------------------------------------------------
app.set("views", path.join(__dirname, "view"));
app.set("view engine", "ejs");
//Home page
app.get("/", (req, res, next) => {
  res.status(200).send("<h1 style='color:red'}>Server is up </h1>");
});
app.use("/creteUsers", require("./routes/createUser"));
app.use("/user", require("./routes/getUsers"));

require("./model/user");
require("./model/image");

module.exports = app;
