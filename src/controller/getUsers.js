const sequelize = require("../util/mysql_database");
const { QueryTypes, Op } = require("sequelize");

exports.getUsers = async (req, res, next) => {
  const userId = req.params.userId;
  sequelize
    .query(
      "SELECT (SELECT COUNT( `users`.`id`) FROM `users`) AS minPosition,`temp`.`position`, `users`.`id`, `users`.`username`,`users`.`Karma_score`, `image`.`id` AS `image.id`, `image`.`url` AS `image.url` FROM `users` JOIN(SELECT `p`.`id`,(@row_number := @row_number + 1) AS `position` FROM `users` as `p` JOIN (SELECT @row_number := 0) `r` ORDER BY `p`.`Karma_score` DESC) AS `temp` ON `users`.`id` = `temp`.`id` LEFT OUTER JOIN `images` AS `image` ON `users`.`imageId` = `image`.`id` WHERE `users`.`id` = :userId ",
      {
        replacements: { userId: userId },
        type: QueryTypes.SELECT,
      }
    )
    .then(async (userArray) => {
      // return res.send(user);
      const user = userArray[0];
      //return res.send(userArray);
      if (user.position == 1) {
        const userHaveBiggerByKarma_score = await sequelize.query(
          "SELECT `temp`.`position`, `users`.`id`, `users`.`username`,`users`.`Karma_score`, `image`.`id` AS `image.id`, `image`.`url` AS `image.url` FROM `users` AS `users`LEFT OUTER JOIN `images` AS `image` ON `users`.`imageId` = `image`.`id` JOIN(SELECT `p`.`id`,(@row_number := @row_number + 1) AS `position` FROM `users` as `p` JOIN (SELECT @row_number := 0) `r` ORDER BY `p`.`Karma_score` DESC) AS `temp` ON `users`.`id` = `temp`.`id` WHERE `users`.`Karma_score` <= ':score' AND `users`.`id` != ':userId' And `temp`.`position`> ':position' ORDER BY `users`.`Karma_score` DESC LIMIT 4",
          {
            replacements: {
              userId: user.id,
              score: user.Karma_score,
              position: user.position,
            },
            type: QueryTypes.SELECT,
          }
        );
        // return res.send(userHaveBiggerByKarma_score);
        return res.render("table", {
          title: "User List",
          userData: [user, ...userHaveBiggerByKarma_score],
        });
      } else if (user.position == user.minPosition) {
        const userHaveSmallerByKarma_score = await sequelize.query(
          "SELECT `temp`.`position`, `users`.`id`, `users`.`username`,`users`.`Karma_score`, `image`.`id` AS `image.id`, `image`.`url` AS `image.url` FROM `users` AS `users`LEFT OUTER JOIN `images` AS `image` ON `users`.`imageId` = `image`.`id` JOIN(SELECT `p`.`id`,(@row_number := @row_number + 1) AS `position` FROM `users` as `p` JOIN (SELECT @row_number := 0) `r` ORDER BY `p`.`Karma_score` DESC) AS `temp` ON `users`.`id` = `temp`.`id` WHERE `users`.`Karma_score` >= ':score' AND `users`.`id` != ':userId' And `temp`.`position`< ':position' ORDER BY `users`.`Karma_score` ASC , `temp`.`position` DESC LIMIT 4",
          {
            replacements: {
              userId: user.id,
              score: user.Karma_score,
              position: user.position,
            },
            type: QueryTypes.SELECT,
          }
        );
        return res.render("table", {
          title: "User List",
          userData: [...userHaveSmallerByKarma_score.reverse(), user],
        });
      } else {
        const userHaveBiggerByKarma_score = await sequelize.query(
          "SELECT `temp`.`position`, `users`.`id`, `users`.`username`,`users`.`Karma_score`, `image`.`id` AS `image.id`, `image`.`url` AS `image.url` FROM `users` AS `users`LEFT OUTER JOIN `images` AS `image` ON `users`.`imageId` = `image`.`id` JOIN(SELECT `p`.`id`,(@row_number := @row_number + 1) AS `position` FROM `users` as `p` JOIN (SELECT @row_number := 0) `r` ORDER BY `p`.`Karma_score` DESC) AS `temp` ON `users`.`id` = `temp`.`id` WHERE `users`.`Karma_score` >= :score AND `users`.`id` != :userId  And `temp`.`position`< ':position' ORDER BY `users`.`Karma_score` ASC ,`temp`.`position` DESC  LIMIT 2",
          {
            replacements: {
              userId: user.id,
              score: user.Karma_score,
              position: user.position,
            },
            type: QueryTypes.SELECT,
          }
        );
        const userHaveSmallerByKarma_score = await sequelize.query(
          "SELECT `temp`.`position`, `users`.`id`, `users`.`username`,`users`.`Karma_score`, `image`.`id` AS `image.id`, `image`.`url` AS `image.url` FROM `users` AS `users`LEFT OUTER JOIN `images` AS `image` ON `users`.`imageId` = `image`.`id` JOIN(SELECT `p`.`id`,(@row_number := @row_number + 1) AS `position` FROM `users` as `p` JOIN (SELECT @row_number := 0) `r` ORDER BY `p`.`Karma_score` DESC) AS `temp` ON `users`.`id` = `temp`.`id` WHERE `users`.`Karma_score` <= :score AND `users`.`id` != :userId  And `temp`.`position`> ':position' ORDER BY `users`.`Karma_score` DESC ,`temp`.`position` ASC  LIMIT 2",
          {
            replacements: {
              userId: user.id,
              score: user.Karma_score,
              position: user.position,
            },
            type: QueryTypes.SELECT,
          }
        );

        return res.render("table", {
          title: "User List",
          userData: [
            ...userHaveBiggerByKarma_score.reverse(),
            user,
            ...userHaveSmallerByKarma_score,
          ],
        });
      }
    })
    .catch((err) => {
      return res.render("table", { title: "User List", userData: [] });
    });
};

exports.getUsersWithLimit = async (req, res, next) => {
  const userId = req.params.userId;
  const limit = req.params.limit - 1;
  sequelize
    .query(
      "SELECT (SELECT COUNT( `users`.`id`) FROM `users`) AS minPosition,`temp`.`position`, `users`.`id`, `users`.`username`,`users`.`Karma_score`, `image`.`id` AS `image.id`, `image`.`url` AS `image.url` FROM `users` JOIN(SELECT `p`.`id`,(@row_number := @row_number + 1) AS `position` FROM `users` as `p` JOIN (SELECT @row_number := 0) `r` ORDER BY `p`.`Karma_score` DESC) AS `temp` ON `users`.`id` = `temp`.`id` LEFT OUTER JOIN `images` AS `image` ON `users`.`imageId` = `image`.`id` WHERE `users`.`id` = :userId ",
      {
        replacements: { userId: userId },
        type: QueryTypes.SELECT,
      }
    )
    .then(async (userArray) => {
      // return res.send(user);
      const user = userArray[0];
      //return res.send(userArray);
      if (user.position == 1) {
        const userHaveBiggerByKarma_score = await sequelize.query(
          "SELECT `temp`.`position`, `users`.`id`, `users`.`username`,`users`.`Karma_score`, `image`.`id` AS `image.id`, `image`.`url` AS `image.url` FROM `users` AS `users`LEFT OUTER JOIN `images` AS `image` ON `users`.`imageId` = `image`.`id` JOIN(SELECT `p`.`id`,(@row_number := @row_number + 1) AS `position` FROM `users` as `p` JOIN (SELECT @row_number := 0) `r` ORDER BY `p`.`Karma_score` DESC) AS `temp` ON `users`.`id` = `temp`.`id` WHERE `users`.`Karma_score` <= ':score' AND `users`.`id` != ':userId' And `temp`.`position`> ':position' ORDER BY `users`.`Karma_score` DESC LIMIT :limit",
          {
            replacements: {
              userId: user.id,
              score: user.Karma_score,
              position: user.position,
              limit: limit,
            },
            type: QueryTypes.SELECT,
          }
        );
        // return res.send(userHaveBiggerByKarma_score);
        return res.render("table", {
          title: "User List",
          userData: [user, ...userHaveBiggerByKarma_score],
        });
      } else if (user.position == user.minPosition) {
        const userHaveSmallerByKarma_score = await sequelize.query(
          "SELECT `temp`.`position`, `users`.`id`, `users`.`username`,`users`.`Karma_score`, `image`.`id` AS `image.id`, `image`.`url` AS `image.url` FROM `users` AS `users`LEFT OUTER JOIN `images` AS `image` ON `users`.`imageId` = `image`.`id` JOIN(SELECT `p`.`id`,(@row_number := @row_number + 1) AS `position` FROM `users` as `p` JOIN (SELECT @row_number := 0) `r` ORDER BY `p`.`Karma_score` DESC) AS `temp` ON `users`.`id` = `temp`.`id` WHERE `users`.`Karma_score` >= ':score' AND `users`.`id` != ':userId' And `temp`.`position`<= ':position' ORDER BY `users`.`Karma_score` ASC , `temp`.`position` DESC LIMIT :limit",
          {
            replacements: {
              userId: user.id,
              score: user.Karma_score,
              position: user.position,
              limit: limit,
            },
            type: QueryTypes.SELECT,
          }
        );
        return res.render("table", {
          title: "User List",
          userData: [...userHaveSmallerByKarma_score.reverse(), user],
        });
      } else {
        if (limit === 0) {
          return res.render("table", { title: "User List", userData: [user] });
        } else {
          var limitOfBigPosition = 0;
          if (limit % 2 == 0) {
            limitOfBigPosition = limit / 2;
          } else {
            limitOfBigPosition = Math.ceil(limit / 2);
          }
          const userHaveBiggerByKarma_score = await sequelize.query(
            "SELECT `temp`.`position`, `users`.`id`, `users`.`username`,`users`.`Karma_score`, `image`.`id` AS `image.id`, `image`.`url` AS `image.url` FROM `users` AS `users`LEFT OUTER JOIN `images` AS `image` ON `users`.`imageId` = `image`.`id` JOIN(SELECT `p`.`id`,(@row_number := @row_number + 1) AS `position` FROM `users` as `p` JOIN (SELECT @row_number := 0) `r` ORDER BY `p`.`Karma_score` DESC) AS `temp` ON `users`.`id` = `temp`.`id` WHERE `users`.`Karma_score` >= :score AND `users`.`id` != :userId  And `temp`.`position`< ':position' ORDER BY `users`.`Karma_score` ASC ,`temp`.`position` DESC  LIMIT :limit",
            {
              replacements: {
                userId: user.id,
                score: user.Karma_score,
                limit: limitOfBigPosition,
                position: user.position,
              },
              type: QueryTypes.SELECT,
            }
          );
          var userHaveSmallerByKarma_score = [];
          const limitOfSmallPosition =
            limit - userHaveBiggerByKarma_score.length;
          if (limitOfSmallPosition != 0) {
            userHaveSmallerByKarma_score = await sequelize.query(
              "SELECT `temp`.`position`, `users`.`id`, `users`.`username`,`users`.`Karma_score`, `image`.`id` AS `image.id`, `image`.`url` AS `image.url` FROM `users` AS `users`LEFT OUTER JOIN `images` AS `image` ON `users`.`imageId` = `image`.`id` JOIN(SELECT `p`.`id`,(@row_number := @row_number + 1) AS `position` FROM `users` as `p` JOIN (SELECT @row_number := 0) `r` ORDER BY `p`.`Karma_score` DESC) AS `temp` ON `users`.`id` = `temp`.`id` WHERE `users`.`Karma_score` <= :score AND `users`.`id` != :userId And `temp`.`position`> ':position' ORDER BY `users`.`Karma_score` DESC ,`temp`.`position` ASC  LIMIT :limit",
              {
                replacements: {
                  userId: user.id,
                  score: user.Karma_score,
                  limit: limitOfSmallPosition,
                  position: user.position,
                },
                type: QueryTypes.SELECT,
              }
            );
          }
          return res.render("table", {
            title: "User List",
            userData: [
              ...userHaveBiggerByKarma_score.reverse(),
              user,
              ...userHaveSmallerByKarma_score,
            ],
          });
        }
      }
    })
    .catch((err) => {
      return res.render("table", { title: "User List", userData: [] });
    });
};
