const sequelize = require("../util/mysql_database");
const { QueryTypes, Op } = require("sequelize");
exports.createUsers = async (req, res, next) => {
  try {
    const numberOfRow = 100000;
    for (let index = 0; index < numberOfRow; index++) {
      var val = Math.floor(1000 + Math.random() * 9000);
      var username = "_" + Math.random().toString(36).slice(2, 9);
      const [imageId, metadata] = await sequelize.query(
        "INSERT INTO `images` (`url`) VALUES (:url)",

        {
          replacements: {
            url: "http://localhost:8000/image/imageName",
          },
          type: QueryTypes.INSERT,
        }
      );
      const [userId] = await sequelize.query(
        "INSERT INTO `users` (`username`, `Karma_score` ,`imageId`) VALUES (:username, :Karma_score, :imageId)",

        {
          replacements: {
            username: username,
            Karma_score: val,
            imageId: imageId,
          },
          type: QueryTypes.INSERT,
        }
      );
    }
    res.status(201).send({ message: "Add successful" });
  } catch (err) {
    res.status(500).send({ error: err });
  }
};
